import torch
import re
import torchaudio
import librosa
import numpy as np
import os
from pathlib import Path
from multiprocessing import Pool
import pandas as pd
from tqdm.auto import tqdm

from datasets import load_dataset, concatenate_datasets
from dataclasses import dataclass
from typing import Any, Dict, List, Optional, Union

from transformers import Wav2Vec2CTCTokenizer
from transformers import Wav2Vec2FeatureExtractor
from transformers import Wav2Vec2Processor

def load_speech(f):
    return torch.load(f).squeeze().tolist()

def get_input_len(f):
    t = torch.load(f).squeeze().tolist()
    return len(t)

class CustomWav2Vec2Dataset(torch.utils.data.Dataset):

        def __init__(self,config, split='train'):
            super().__init__()
            assert split in {'train', 'eval'}
            self.split = split
            self.path = Path(f'./vi.{split}.parquet')
            df = pd.read_parquet(self.path)
            self.labels = [x.tolist() for x in df['labels'].tolist()]
            self.paths = df['path'].tolist()
            self.max_input_length_quantile = .98
            self.max_input_length = None

            if split == 'train':
                with Pool(config['num_loader_workers']) as p:
                    self.input_seq_lengths = list(
                        tqdm(p.imap(get_input_len, self.paths), total=len(self.paths), miniters=100,
                             desc='getting train input lengths'))
                self.max_input_length = torch.tensor(self.input_seq_lengths).float().quantile(
                    self.max_input_length_quantile).int().item()

        def __len__(self):
            return len(self.paths)

        def __getitem__(self, idx):
            inputs = load_speech(self.paths[idx])
            if self.split == 'train':
                inputs = inputs[:self.max_input_length]
            label = self.labels[idx]
            return {'input_values': inputs, 'labels': label}


class Dataset(object):
    def __init__(self, config, vocab, text_column='text', audio_path_column='audio_path'):
        self.config = config
        self.text_column = text_column
        self.audio_path_column = audio_path_column
        self.vocab = vocab
        # load datasets
        self.train_dataset = None
        self.devel_dataset = None

        self.files_path = self.config.datasets['files_path'] if 'files_path'in self.config['datasets'].keys() else None

        self.tokenizer = Wav2Vec2CTCTokenizer(self.config.vocab['vocab_path'], unk_token=self.config.vocab['unk'], pad_token=self.config.vocab['blank'], word_delimiter_token=self.config.vocab['silence'])
        self.feature_extractor = Wav2Vec2FeatureExtractor(feature_size=1, sampling_rate=self.config['sampling_rate'], padding_value=0.0, do_normalize=True, return_attention_mask=True)
        self.processor = Wav2Vec2Processor(feature_extractor=self.feature_extractor, tokenizer=self.tokenizer)


    def preprocess_datasets(self):
        self.train_dataset = CustomWav2Vec2Dataset(self.config, 'train')
        self.devel_dataset = CustomWav2Vec2Dataset(self.config, 'eval')

    
@dataclass
class DataColletor:
    # Adpated from https://huggingface.co/blog/fine-tune-xlsr-wav2vec2
    """
    Data collator that will dynamically pad the inputs received.
    Args:
        processor (:class:`~transformers.Wav2Vec2Processor`)
            The processor used for proccessing the data.
        padding (:obj:`bool`, :obj:`str` or :class:`~transformers.tokenization_utils_base.PaddingStrategy`, `optional`, defaults to :obj:`True`):
            Select a strategy to pad the returned sequences (according to the model's padding side and padding index)
            among:
            * :obj:`True` or :obj:`'longest'`: Pad to the longest sequence in the batch (or no padding if only a single
              sequence if provided).
            * :obj:`'max_length'`: Pad to a maximum length specified with the argument :obj:`max_length` or to the
              maximum acceptable input length for the model if that argument is not provided.
            * :obj:`False` or :obj:`'do_not_pad'` (default): No padding (i.e., can output a batch with sequences of
              different lengths).
        max_length (:obj:`int`, `optional`):
            Maximum length of the ``input_values`` of the returned list and optionally padding length (see above).
        max_length_labels (:obj:`int`, `optional`):
            Maximum length of the ``labels`` returned list and optionally padding length (see above).
        pad_to_multiple_of (:obj:`int`, `optional`):
            If set will pad the sequence to a multiple of the provided value.
            This is especially useful to enable the use of Tensor Cores on NVIDIA hardware with compute capability >=
            7.5 (Volta).
    """
    def __init__(self, processor, audio_augmentator=None, sampling_rate=16000, padding=True, test=False, max_length=None, max_length_labels=None, pad_to_multiple_of=None, pad_to_multiple_of_labels=None):
        self.processor = processor
        self.audio_augmentator = audio_augmentator
        self.sampling_rate = sampling_rate
        self.padding = padding
        self.test = test
        self.max_length = max_length
        self.max_length_labels = max_length_labels
        self.pad_to_multiple_of = pad_to_multiple_of
        self.pad_to_multiple_of_labels = pad_to_multiple_of_labels

    def __call__(self, features: List[Dict[str, Union[List[int], torch.Tensor]]]) -> Dict[str, torch.Tensor]:
        # split inputs and labels since they have to be of different lenghts and need
        # different padding methods
        input_features = []
        label_features = []
        audio_paths = []
        for feature in features:
            if self.audio_augmentator is not None:
                input_tensor = self.audio_augmentator(np.array(feature["input_values"]), sample_rate=self.sampling_rate).tolist()
            else:
                input_tensor = feature["input_values"]

            input_features.append({"input_values":input_tensor})
            label_features.append({"input_ids": feature["labels"]})

            if self.test:
                audio_paths.append(feature['audio_path'])

        batch = self.processor.pad(
            input_features,
            padding=self.padding,
            max_length=self.max_length,
            pad_to_multiple_of=self.pad_to_multiple_of,
            return_tensors="pt",
        )
        with self.processor.as_target_processor():
            labels_batch = self.processor.pad(
                label_features,
                padding=self.padding,
                max_length=self.max_length_labels,
                pad_to_multiple_of=self.pad_to_multiple_of_labels,
                return_tensors="pt",
            )

        # replace padding with -100 to ignore loss correctly
        labels = labels_batch["input_ids"].masked_fill(labels_batch.attention_mask.ne(1), -100)

        batch["labels"] = labels
        if self.test:
            batch["audio_path"] = audio_paths
        return batch

'''if __name__ == "__main__":
    from generic_utils import load_config, load_vocab
    config_path = 'example/config_example.json'

    config = load_config(config_path)
    vocab = load_vocab(config.vocab['vocab_path'])
    dataset = Dataset(config, vocab)'''