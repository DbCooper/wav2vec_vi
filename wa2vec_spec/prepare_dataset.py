import glob
import json
import os
import random
import re
import sys
from pathlib import Path

import datasets
import pyarrow.parquet as pq
import torch
import torchaudio
from datasets import load_dataset
from tqdm.auto import tqdm
from transformers import (
    HfArgumentParser,
    TrainingArguments,
    Wav2Vec2CTCTokenizer,
    Wav2Vec2FeatureExtractor,
    Wav2Vec2Processor,
)
import argparse
from utils.generic_utils import load_config, load_vocab

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config_path', type=str, required=True,
                    help="json file with configurations")
parser.add_argument('--checkpoint_path', type=str, default='facebook/wav2vec2-large-xlsr-53',
                    help="path of checkpoint pt file, for continue training")
args = parser.parse_args()

config = load_config(args.config_path)


def load_info(path: str) -> dict:
    # get length of file in samples
    info = {}
    if torchaudio.get_audio_backend() == "sox_io":
        si = torchaudio.info(str(path))
        info['samplerate'] = si.sample_rate
        info['samples'] = si.num_frames
    else:
        si, _ = torchaudio.info(str(path))
        info['samplerate'] = si.rate
        if torchaudio.get_audio_backend() == "sox":
            info['samples'] = si.length // si.channels
        else:
            # soundfile and sox_io calc per channel
            info['samples'] = si.length

    info['duration'] = info['samples'] / info['samplerate']
    return info


# increasing number of threads for torchaudio resample
print(f'Using {config.num_loader_workers} threads')
torch.set_num_threads(config.num_loader_workers)

with open(f'{config.custom_data_folder}/metadata.csv', 'r', encoding='utf-8') as file:
    lines = file.readlines()

# Shuffle Dataset
for i in range(10):
    random.shuffle(lines)

tran_dict = dict()
for line in lines:
    delete = line.strip()
    tokens = delete.split("|")
    key = tokens[0]
    trn = ''.join(tokens[1])
    tran_dict[key] = trn.lower()

samples = []
filenames = glob.glob(f"{config.custom_data_folder}/wavs/*")
for f in filenames:
    duration = load_info(f)
    if duration['duration'] < 1 or duration['duration'] > 17.0: continue
    key = os.path.basename(f)
    if key in tran_dict:
        trn = tran_dict[key]
        samples.append({'path': f, 'sentence': trn})

Path("dummy_dataset").mkdir(parents=True, exist_ok=True)

for i, sample in enumerate(samples):
    with open(f'dummy_dataset/sample_{i}.json', 'w', encoding='utf-8') as outfile:
        json.dump(sample, outfile, ensure_ascii=False)
split = int(len(samples) * 0.8)

dummy_train_dataset = load_dataset("json",
                                   data_files=[f"dummy_dataset/sample_{i}.json" for i in range(0, split)],
                                   split="train")
dummy_eval_dataset = load_dataset("json",
                                  data_files=[f"dummy_dataset/sample_{i}.json" for i in range(split, len(samples))],
                                  split="train")

train_dataset = dummy_train_dataset
eval_dataset = dummy_eval_dataset


def vocab_to_string(vocab, blank, silence, unk, space=' '):
    vocab_list = list(vocab.keys())
    # remove special tokens
    vocab_list = [x if len(x) == 1 else '' for x in vocab_list]
    vocab_list.sort()
    # remove special with len 1
    vocab_list.remove(silence)
    # vocab_list.remove(blank)
    # vocab_list.remove(unk)

    # append space token
    vocab_list.append(space)
    # convert to string
    return ''.join(vocab_list)


vocab = load_vocab(config.vocab['vocab_path'])
vocab_string = vocab_to_string(vocab, config.vocab['blank'], config.vocab['silence'], config.vocab['unk'])


def remove_special_characters(batch):
    text = batch["text"].lower()
    text = re.sub("[^{}]".format(vocab_string), " ", text)
    text = re.sub("[ ]+", " ", text)
    batch["text"] = text + " "
    return batch


train_dataset = train_dataset.map(remove_special_characters, remove_columns=["sentence"], keep_in_memory=True,
                                  num_proc=config.num_loader_workers)
eval_dataset = eval_dataset.map(remove_special_characters, remove_columns=["sentence"], keep_in_memory=True,
                                num_proc=config.num_loader_workers)

if args.max_train_samples is not None:
    if split > int(args.max_train_samples):
        train_dataset = train_dataset.select(range(args.max_train_samples))
    else:
        train_dataset = train_dataset.select(range(split))

if args.max_val_samples is not None:
    if (len(samples) - split) > int(args.max_val_samples):
        eval_dataset = eval_dataset.select(range(args.max_val_samples))
    else:
        val_split = len(samples) - split
        train_dataset = train_dataset.select(range(val_split))

# Load pretrained tokenizer & create processor
tokenizer = Wav2Vec2CTCTokenizer(config.vocab['vocab_path'], unk_token="<unk>", pad_token="<pad>",
                                 word_delimiter_token="|")
feature_extractor = Wav2Vec2FeatureExtractor(feature_size=1, sampling_rate=16_000, padding_value=0.0, do_normalize=True,
                                             return_attention_mask=True)
processor = Wav2Vec2Processor(feature_extractor=feature_extractor, tokenizer=tokenizer)

# =======================================================
# The following part is modified to:
#   - load, resample, process and save audio files into raw tensors
#   - process labels as before
#   - save datasets containing the paths and labels to disk, as arrow table in parquet format
#   - save processor to disk, for reuse in training script (I got a hint that it is not deterministic)

# load and resample audio, save as raw tensors
resampled_data_dir = Path('./resampled')
resampled_data_dir.mkdir(exist_ok=True)


def load_resample_save(f):
    # sr = sox.file_info.sample_rate(f)
    # sr = int(sr)
    f = Path(f)
    new_path = resampled_data_dir / f'{f.stem}_resampled16k.pt'
    if not new_path.exists():
        # sr = sox.file_info.sample_rate(f["path"])
        duration = load_info(f)['duration']
        speech_array, sampling_rate = torchaudio.load(f)
        resampler = torchaudio.transforms.Resample(int(sampling_rate), 16_000)
        speech_array_resampled = resampler(speech_array)
        input_values = processor(speech_array_resampled, sampling_rate=16_000).input_values
        input_values = torch.from_numpy(input_values).float().flatten()
        torch.save(input_values, new_path)
    return str(new_path)


print('load resample save')
new_train_paths = [load_resample_save(f) for f in tqdm(train_dataset['path'], miniters=100, desc='train')]
new_eval_paths = [load_resample_save(f) for f in tqdm(eval_dataset['path'], miniters=100, desc='eval')]

# update paths and sampling rate
train_dataset = train_dataset.map(
    lambda x: {'path': new_train_paths, 'sampling_rate': [16_000] * len(train_dataset), 'target_text': x['text']},
    batched=True,
    batch_size=-1,
    keep_in_memory=True,
    remove_columns=train_dataset.column_names,
)
eval_dataset = eval_dataset.map(
    lambda x: {'path': new_eval_paths, 'sampling_rate': [16_000] * len(eval_dataset), 'target_text': x['text']},
    batched=True,
    batch_size=-1,
    keep_in_memory=True,
    remove_columns=eval_dataset.column_names,
)


# tokenize targets
def tokenize_targets(batch):
    # Setup the processor for targets
    with processor.as_target_processor():
        batch["labels"] = processor(batch["target_text"]).input_ids
    return batch


print('preparing dataset: train')
train_dataset = train_dataset.map(
    tokenize_targets,
    remove_columns=[col for col in train_dataset.column_names if col != 'path'],
    batch_size=8,
    batched=True,
    num_proc=config.num_loader_workers,
)
print('preparing dataset: eval')
eval_dataset = eval_dataset.map(
    tokenize_targets,
    remove_columns=[col for col in eval_dataset.column_names if col != 'path'],
    batch_size=8,
    batched=True,
    num_proc=config.num_loader_workers,
)

# # save for disk, ready for training
pq.write_table(train_dataset.data, f'./vi.train.parquet')
pq.write_table(eval_dataset.data, f'./vi.eval.parquet')

# save processor for training
# processor.save_pretrained(training_args.output_dir)
