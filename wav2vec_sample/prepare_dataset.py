import glob
import json
import os
import random
import re
import sys
from pathlib import Path

import datasets
import numpy as np
import pyarrow.parquet as pq
import torch
import torchaudio
from datasets import load_dataset
from loguru import logger
from tqdm import tqdm
from tqdm.auto import tqdm
from transformers import (HfArgumentParser, TrainingArguments,
                          UniSpeechSatFeatureExtractor, Wav2Vec2CTCTokenizer,
                          Wav2Vec2FeatureExtractor, Wav2Vec2Processor)

from argument_classes import AttrDict, DataTrainingArguments, ModelArguments

mode_feature = os.getenv("mode_feature","w2v") # w2v, unisat
features = {
    "w2v": Wav2Vec2FeatureExtractor,
    "uni": Wav2Vec2FeatureExtractor,
    "unisat": UniSpeechSatFeatureExtractor,
}

def load_info(path: str) -> dict:
    # get length of file in samples
    info = {}
    if torchaudio.get_audio_backend() == "sox_io":
        si = torchaudio.info(str(path))
        info['samplerate'] = si.sample_rate
        info['samples'] = si.num_frames
    else:
        si, _ = torchaudio.info(str(path))
        info['samplerate'] = si.rate
        if torchaudio.get_audio_backend() == "sox":
            info['samples'] = si.length // si.channels
        else:
            # soundfile and sox_io calc per channel
            info['samples'] = si.length

    info['duration'] = info['samples'] / info['samplerate']
    return info


parser = HfArgumentParser((ModelArguments, DataTrainingArguments, TrainingArguments))
if len(sys.argv) == 2 and sys.argv[1].endswith(".json"):
    # If we pass only one argument to the script and it's the path to a json file,
    # let's parse it to get our arguments.
    model_args, data_args, training_args = parser.parse_json_file(json_file=os.path.abspath(sys.argv[1]))
else:
    model_args, data_args, training_args = parser.parse_args_into_dataclasses()

# increasing number of threads for torchaudio resample
logger.info(f'Using {data_args.preprocessing_num_workers} threads')
torch.set_num_threads(data_args.preprocessing_num_workers)

with open(f'{data_args.custom_data_folder}/metadata.csv', 'r', encoding='utf-8') as file:
    lines = file.readlines()

# Shuffle Dataset
for i in range(10):
    random.shuffle(lines)

logger.info(f"Total row in metadata: {len(lines)}")
logger.info("Generate trans dict")
tran_dict = dict()
for line in tqdm(lines):
    delete = line.strip()
    tokens = delete.split("|")
    key = tokens[0]
    trn = ''.join(tokens[1])
    tran_dict[key] = trn.lower()

logger.info("Generate json data")
samples = []
filenames = glob.glob(f"{data_args.custom_data_folder}/wavs/*")
for f in tqdm(filenames):
    duration = load_info(f)
    if duration['duration'] < 1 or duration['duration'] > 20.0: continue
    key = os.path.basename(f)
    #key = f'wavs/{key}'
    if key in tran_dict:
        trn = tran_dict[key]
        samples.append({'path': f, 'sentence': trn})

logger.info("Dummy datasets")

Path("dummy_dataset").mkdir(parents=True, exist_ok=True)
for i, sample in enumerate(samples):
    with open(f'dummy_dataset/sample_{i}.json', 'w', encoding='utf-8') as outfile:
        json.dump(sample, outfile, ensure_ascii=False)

split = int(len(samples) * 0.9)
logger.info(f"Length train dataset: {split}- Length eval dataset: {len(samples)-split}")
logger.info("Starting load datasets")
logger.info("=================> Load training data ...")
train_dataset = load_dataset("json",
                                   data_files=[f"dummy_dataset/sample_{i}.json" for i in range(0, split)],
                                   split="train")
logger.info("=================> Load eval data...")
eval_dataset = load_dataset("json",
                                  data_files=[f"dummy_dataset/sample_{i}.json" for i in range(split, len(samples))],
                                  split="train")
logger.info("End load datasets")

# Create and save tokenizer
logger.info("Remove special characters")
chars_to_ignore_regex = f'[{"".join(data_args.chars_to_ignore)}]'

def remove_special_characters(batch):
    batch["text"] = re.sub(chars_to_ignore_regex, "", batch["sentence"]).lower() + " "
    return batch

train_dataset = train_dataset.map(remove_special_characters, remove_columns=["sentence"], keep_in_memory=True,
                                  num_proc=data_args.preprocessing_num_workers)
eval_dataset = eval_dataset.map(remove_special_characters, remove_columns=["sentence"], keep_in_memory=True,
                                num_proc=data_args.preprocessing_num_workers)

logger.info("Extract vocab")

def extract_all_chars(batch):
    all_text = " ".join(batch["text"])
    vocab = list(set(all_text))
    return {"vocab": [vocab], "all_text": [all_text]}

vocab_train = train_dataset.map(
    extract_all_chars,
    batched=True,
    batch_size=-1,
    keep_in_memory=True,
    remove_columns=train_dataset.column_names,
)
vocab_test = train_dataset.map(
    extract_all_chars,
    batched=True,
    batch_size=-1,
    keep_in_memory=True,
    remove_columns=eval_dataset.column_names,
)

vocab_list = list(set(vocab_train["vocab"][0]) | set(vocab_test["vocab"][0]))
vocab_dict = {v: k for k, v in enumerate(vocab_list)}
vocab_dict["|"] = vocab_dict[" "]
del vocab_dict[" "]
vocab_dict["[unk]"] = len(vocab_dict)
vocab_dict["[pad]"] = len(vocab_dict)

with open("vocab.json", "w") as vocab_file:
    json.dump(vocab_dict, vocab_file)


if data_args.max_train_samples is not None:
    if split > int(data_args.max_train_samples):
        train_dataset = train_dataset.select(range(data_args.max_train_samples))
    else:
        train_dataset = train_dataset.select(range(split))

if data_args.max_val_samples is not None:
    if (len(samples) - split) > int(data_args.max_val_samples):
        eval_dataset = eval_dataset.select(range(data_args.max_val_samples))
    else:
        val_split = len(samples) - split
        eval_dataset = eval_dataset.select(range(val_split))

# Load pretrained tokenizer & create processor
logger.info("Load pretrained tokenizer & create processor")
tokenizer = Wav2Vec2CTCTokenizer(
    "vocab.json",
    unk_token="[unk]",
    pad_token="[pad]",
    word_delimiter_token="|",
)

feature_extractor = features.get(mode_feature)(
    feature_size=1, sampling_rate=16_000, padding_value=0.0, do_normalize=True, return_attention_mask=True
)

processor = Wav2Vec2Processor(feature_extractor=feature_extractor, tokenizer=tokenizer)
# =======================================================
# The following part is modified to:
#   - load, resample, process and save audio files into raw tensors
#   - process labels as before
#   - save datasets containing the paths and labels to disk, as arrow table in parquet format
#   - save processor to disk, for reuse in training script (I got a hint that it is not deterministic)

# load and resample audio, save as raw tensors
resampled_data_dir = Path('./resampled')
resampled_data_dir.mkdir(exist_ok=True)


def load_resample_save(f):
    # sr = sox.file_info.sample_rate(f)
    # sr = int(sr)
    f = Path(f)
    new_path = resampled_data_dir / f'{f.stem}_resampled16k.pt'
    if not new_path.exists():
        # sr = sox.file_info.sample_rate(f["path"])
        speech_array, sampling_rate = torchaudio.load(f)
        resampler = torchaudio.transforms.Resample(int(sampling_rate), 16_000)
        speech_array_resampled = resampler(speech_array)
        input_values = processor(speech_array_resampled, sampling_rate=16_000).input_values
        input_values = torch.from_numpy(np.array(input_values)).float().flatten()
        torch.save(input_values, new_path)
    return str(new_path)


logger.info('load resample save')
new_train_paths = [load_resample_save(f) for f in tqdm(train_dataset['path'], miniters=100, desc='train')]
new_eval_paths = [load_resample_save(f) for f in tqdm(eval_dataset['path'], miniters=100, desc='eval')]

# update paths and sampling rate
logger.info("update paths and sampling rate")
train_dataset = train_dataset.map(
    lambda x: {'path': new_train_paths, 'sampling_rate': [16_000] * len(train_dataset), 'target_text': x['text']},
    batched=True,
    batch_size=-1,
    keep_in_memory=True,
    remove_columns=train_dataset.column_names,
)
eval_dataset = eval_dataset.map(
    lambda x: {'path': new_eval_paths, 'sampling_rate': [16_000] * len(eval_dataset), 'target_text': x['text']},
    batched=True,
    batch_size=-1,
    keep_in_memory=True,
    remove_columns=eval_dataset.column_names,
)


# tokenize targets
def tokenize_targets(batch):
    # check that all files have the correct sampling rate
    # assert (
    #         len(set(batch["sampling_rate"])) == 1
    #     ), f"Make sure all inputs have the same sampling rate of {processor.feature_extractor.sampling_rate}."
    # batch["input_values"] = processor(batch["speech"], sampling_rate=batch["sampling_rate"][0]).input_values
    # Setup the processor for targets
    with processor.as_target_processor():
        batch["labels"] = processor(batch["target_text"]).input_ids
    return batch


logger.info('preparing dataset: train')
train_dataset = train_dataset.map(
    tokenize_targets,
    remove_columns=[col for col in train_dataset.column_names if col != 'path'],
    batch_size=training_args.per_device_train_batch_size,
    batched=True,
    num_proc=data_args.preprocessing_num_workers,
)
logger.info('preparing dataset: eval')
eval_dataset = eval_dataset.map(
    tokenize_targets,
    remove_columns=[col for col in eval_dataset.column_names if col != 'path'],
    batch_size=training_args.per_device_train_batch_size,
    batched=True,
    num_proc=data_args.preprocessing_num_workers,
)

# # save for disk, ready for training
pq.write_table(train_dataset.data.table, f'./{data_args.dataset_config_name}.train.parquet')
pq.write_table(eval_dataset.data.table, f'./{data_args.dataset_config_name}.eval.parquet')

# save processor for training
processor.save_pretrained(training_args.output_dir)
